package com.peaas.controllers;

import java.util.List;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.peaas.commands.CreateUserProfileCommand;
import com.peaas.models.User;
import com.peaas.services.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private CommandGateway commandGateway;

	@GetMapping
	public List<User> findAllUser() {
		return userService.findAll();
	}

	@PostMapping
	public User createUserProfile(@RequestBody User obj) {
		User user = null;
		try {
			user = userService.save(obj);			
			CreateUserProfileCommand command = new CreateUserProfileCommand(user.getId(), user.getName(),
					user.getSurname(), user.getAddress(), user.getEmail());
			commandGateway.send(command);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return user;
	}
}
