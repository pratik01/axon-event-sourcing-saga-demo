package com.peaas.aggregates;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import com.peaas.commands.CreateUserProfileCommand;
import com.peaas.commands.UserCreatedSuccessCommand;
import com.peaas.events.CreateUserProfileEvent;
import com.peaas.events.UserCreatedSuccessEvent;

@Aggregate
public class UserProfileAggregate {

	@AggregateIdentifier
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;
	private String name;
	private String surname;
	private String address;
	private String email;

	@CommandHandler
	public UserProfileAggregate(CreateUserProfileCommand command) {
		System.out.println("--- Command Handler Start : Create User Profile command  ---");
		AggregateLifecycle.apply(new CreateUserProfileEvent(command.getUserId(), command.getName(),
				command.getSurname(), command.getAddress(), command.getEmail()));
		System.out.println("--- Command Handler End : Create User Profile command ---");
	}

	@EventSourcingHandler
	public void handle(CreateUserProfileEvent event) {
		System.out.println("--- Event Sourcing Handler Start : Create User Profile Event  ---");
		this.userId = event.getUserId();
		this.name = event.getName();
		this.surname = event.getSurname();
		this.address = event.getAddress();
		this.email = event.getEmail();
		System.out.println("--- Event Sourcing Handler End : Create User Profile Event  ---");
	}

	@CommandHandler
	public UserProfileAggregate(UserCreatedSuccessCommand command) {
		System.out.println("--- Command Handler Start : Create User Profile command  ---");
		AggregateLifecycle.apply(new UserCreatedSuccessEvent(command.getUserId(), command.getName(),
				command.getSurname(), command.getAddress(), command.getEmail()));
		System.out.println("--- Command Handler End : Create User Profile command ---");
	}

	@EventSourcingHandler
	public void handle(UserCreatedSuccessEvent event) {
		System.out.println("--- Event Sourcing Handler Start : Create User Profile Event  ---");
		this.userId = event.getUserId();
		this.name = event.getName();
		this.surname = event.getSurname();
		this.address = event.getAddress();
		this.email = event.getEmail();
		System.out.println("--- Event Sourcing Handler End : Create User Profile Event  ---");
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserProfileAggregate [userId=" + userId + ", name=" + name + ", surname=" + surname + ", address="
				+ address + ", email=" + email + "]";
	}

}
