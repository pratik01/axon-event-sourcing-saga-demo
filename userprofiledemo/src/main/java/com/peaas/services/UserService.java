package com.peaas.services;

import java.util.List;

import com.peaas.models.User;

public interface UserService {
	public User save(User obj);

	public List<User> findAll();
}
