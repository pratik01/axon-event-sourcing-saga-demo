package com.peaas.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peaas.models.User;
import com.peaas.repositoy.UserRepository;
import com.peaas.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;

	public User save(User obj) {
		// TODO Auto-generated method stub
		return userRepo.save(obj);
	}

	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userRepo.findAll();
	}

}
