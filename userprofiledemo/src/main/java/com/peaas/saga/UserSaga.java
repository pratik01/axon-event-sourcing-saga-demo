package com.peaas.saga;

import static org.axonframework.commandhandling.GenericCommandMessage.asCommandMessage;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.callbacks.LoggingCallback;
import org.axonframework.eventhandling.saga.EndSaga;
import org.axonframework.eventhandling.saga.SagaEventHandler;
import org.axonframework.eventhandling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import com.peaas.commands.UserCreatedSuccessCommand;
import com.peaas.events.CreateUserProfileEvent;
import com.peaas.events.WalletCreatedEvent;

@Saga
public class UserSaga {

	private transient CommandBus commandBus;

	@Autowired
	public void setCommandBus(CommandBus commandBus) {
		this.commandBus = commandBus;
	}

	private Long userId;

	@StartSaga
	@SagaEventHandler(associationProperty = "userId")
	public void on(CreateUserProfileEvent event) {
		System.out.println(event.toString());
		this.userId = 2l;
		System.out.println("\n\nSaga : This is sourcebank accountid:: " + this.userId + "\n\n");
		UserCreatedSuccessCommand command = new UserCreatedSuccessCommand(this.userId, event.getName(),
				event.getSurname(), event.getAddress(), event.getEmail());
		System.out.println("\n\nSaga : Command Obj : " + command.toString());
		commandBus.dispatch(asCommandMessage(command), LoggingCallback.INSTANCE);
	}

	@SagaEventHandler(associationProperty = "userId", payloadType = WalletCreatedEvent.class)
	@EndSaga
	public void handle(WalletCreatedEvent event) {
		System.out.println("\n\nSaga : Wallet Created Successfully");
	}
}
