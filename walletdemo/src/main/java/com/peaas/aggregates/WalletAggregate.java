package com.peaas.aggregates;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import com.peaas.commands.WalletCreatedCommand;
import com.peaas.events.WalletCreatedEvent;

@Aggregate
public class WalletAggregate {

	private Long id;
	@AggregateIdentifier // annotation tells Axon that annotated field will be
							// used as identifier of the Aggregate.
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String walletId;
	private Double amount;
	private Long userId;

	public WalletAggregate() {

	}

	@CommandHandler
	public WalletAggregate(WalletCreatedCommand command) {
		System.out.println("\n\nAggregate : Wallet Command Handler +++++");
		AggregateLifecycle.apply(new WalletCreatedEvent(command.getId(), command.getWalletId(), command.getAmount(),
				command.getUserId()));
	}

	@EventSourcingHandler
	public void handle(WalletCreatedEvent event) {
		this.id = event.getId();
		this.userId = event.getUserId();
		this.walletId = event.getWalletId();
		this.amount = event.getAmount();
		System.out.println("\n\nAggregate : Wallet Event Sourcing +++++");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WalletAggregate [id=" + id + ", walletId=" + walletId + ", amount=" + amount + ", userId=" + userId
				+ "]";
	}

}
