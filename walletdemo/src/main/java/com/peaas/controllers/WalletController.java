package com.peaas.controllers;

import java.util.List;
import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.peaas.models.Wallet;
import com.peaas.services.WalletService;

@RestController
@RequestMapping("/api/wallets")
public class WalletController {

	@Autowired
	private WalletService walletService;

	@Autowired	
	private CommandGateway commandGateway;

	@GetMapping
	public List<Wallet> findAll() {
		return walletService.findAll();
	}

	@PostMapping
	public Wallet save(@RequestBody Wallet obj) {
		Wallet wallet = null;
		try {
			wallet = walletService.save(obj);
			String walletId = UUID.randomUUID().toString();
//			WalletCreatedCommand command = new WalletCreatedCommand(wallet.getId(), walletId, wallet.getAmount(),
//					wallet.getUserid());
//			commandGateway.send(command);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return wallet;
	}
}
