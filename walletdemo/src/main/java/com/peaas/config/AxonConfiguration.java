package com.peaas.config;

import org.axonframework.amqp.eventhandling.DefaultAMQPMessageConverter;
import org.axonframework.amqp.eventhandling.spring.SpringAMQPMessageSource;
import org.axonframework.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import com.rabbitmq.client.Channel;

@Configuration

public class AxonConfiguration {

	private final static Logger logger = LoggerFactory.getLogger(AxonConfiguration.class);

	@Value("${axon.amqp.exchange}")
	private String exchange;

	@Bean
	public Exchange exchange() {
		logger.info(exchange + " AMQP Exchange Registering ");
		return ExchangeBuilder.fanoutExchange(exchange).build();
	}

	@Bean
	public Queue queue() {
		return QueueBuilder.durable(exchange).build();
	}

	@Bean
	public Binding binding() {
		return BindingBuilder.bind(queue()).to(exchange()).with("*").noargs();
	}

	@Autowired
	public void configure(AmqpAdmin amqpAdmin) {
		amqpAdmin.declareExchange(exchange());
		amqpAdmin.declareQueue(queue());
		amqpAdmin.declareBinding(binding());
	}

	@Bean
	public SpringAMQPMessageSource pesss_user(Serializer serializer) {

		return new SpringAMQPMessageSource(new DefaultAMQPMessageConverter(serializer)) {

			@RabbitListener(queues = "pesss_user")

			@Transactional
			@Override
			public void onMessage(Message message, Channel channel) throws Exception {
				System.out.println(message.getMessageProperties());
				System.out.println(channel);
				super.onMessage(message, channel);
			}
		};
	}
}
