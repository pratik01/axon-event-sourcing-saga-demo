package com.peaas.services;

import java.util.List;

import com.peaas.models.Wallet;

public interface WalletService {
	
	public Wallet save(Wallet obj);
	
	public List<Wallet> findAll();
}
