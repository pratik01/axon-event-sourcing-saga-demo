package com.peaas.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peaas.models.Wallet;
import com.peaas.repository.WalletRepository;
import com.peaas.services.WalletService;

@Service
public class WalletServiceImpl implements WalletService {

	@Autowired
	private WalletRepository walletRepo;

	public Wallet save(Wallet obj) {
		// TODO Auto-generated method stub
		return walletRepo.save(obj);
	}

	public List<Wallet> findAll() {
		// TODO Auto-generated method stub
		return walletRepo.findAll();
	}

}
