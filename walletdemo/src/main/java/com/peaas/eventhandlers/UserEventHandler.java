package com.peaas.eventhandlers;

import java.util.UUID;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peaas.commands.WalletCreatedCommand;
import com.peaas.events.UserCreatedSuccessEvent;
import com.peaas.models.Wallet;
import com.peaas.services.WalletService;

@ProcessingGroup("amqpEvents")
@Component
public class UserEventHandler {

	@Autowired
	private transient CommandGateway commandGateway;

	@Autowired
	private WalletService walletService;

	public UserEventHandler() {

	}

	@EventHandler
	public void handleEvent(UserCreatedSuccessEvent event) {
		System.out.println("++++ Wallet : CreateUserProfile Event ++++");
		Wallet obj = new Wallet();
		obj.setUserid(event.getUserId());
		obj.setAmount(0d);
		obj = walletService.save(obj);
		String walletId = UUID.randomUUID().toString();
		WalletCreatedCommand command = new WalletCreatedCommand(obj.getId(), walletId, obj.getAmount(),
				obj.getUserid());
		commandGateway.send(command);
	}
}
