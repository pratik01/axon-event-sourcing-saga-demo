package com.peaas.events;

public class UserCreatedSuccessEvent {
	private Long userId;
	private String name;
	private String surname;
	private String address;
	private String email;

	public UserCreatedSuccessEvent() {

	}

	public UserCreatedSuccessEvent(Long userId, String name, String surname, String address, String email) {
		super();
		this.userId = userId;
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.email = email;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserCreatedSuccessEvent [userId=" + userId + ", name=" + name + ", surname=" + surname + ", address="
				+ address + ", email=" + email + "]";
	}

}
