package com.peaas.commands;

import org.axonframework.commandhandling.TargetAggregateIdentifier;

public class WalletCreatedCommand {
	private Long id;
	@TargetAggregateIdentifier
	private String walletId;
	private Double amount;
	private Long userId;

	public WalletCreatedCommand() {

	}

	public WalletCreatedCommand(Long id, String walletId, Double amount, Long userId) {
		super();
		this.id = id;
		this.walletId = walletId;
		this.amount = amount;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	@Override
	public String toString() {
		return "WalletCreatedCommand [id=" + id + ", walletId=" + walletId + ", amount=" + amount + ", userId=" + userId
				+ "]";
	}

}
